import {configureStore, combineReducers} from '@reduxjs/toolkit';
import {persistStore, persistReducer} from 'redux-persist';
import userSlice from '../features/userSlice';
import storage from 'redux-persist/lib/storage';
import {createFilter} from 'redux-persist-transform-filter';

// savedUserOnlyFilter
const savedUserOnlyFilter = createFilter('user', ['user']);


// persist configuration
const persistConfig = {
  key: 'user',
  storage,
  whitelist: ['user'],
  transforms: [savedUserOnlyFilter],
};

const rootReducer = combineReducers({
  user: userSlice
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false
  }),
  devTools: true,
});

export const persistor = persistStore(store);




