import mongoose from "mongoose";
import validator from "validator";
import bcrypt from "bcryptjs";

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please provide your name'],
  },
  email: {
    type: String,
    required: [true, 'Please provide your email'],
    unique: [true, 'This email already exists'],
    lowercase: true,
    validade: [validator.isEmail, 'Please provide a valid email address'],
  },
  picture: {
    type: String,
    default:
      'https://res.cloudinary.com/dkd5jblv5/image/upload/v1675976806/Default_ProfilePicture_gjngnb.png',
  },
  status: {
    type: String,
    default: 'Hey there! I am using whatsapp',
  },
  password: {
    type: String,
    required: [true, 'Please provide a password'],
    minlength: [
      6,
      'Plase make sure your password is at least 6 characters long',
    ],
    maxlength: [
      128,
      'Plase make sure your password is less than 128 characters long',
    ],
  },
}, {
  collection: 'users',
  timestamps: true,
});

UserSchema.pre('save', async function (next) {
  try {
    if(this.isNew) {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(this.password, salt);
      this.password = hash;
      next();
    }
  } catch(error) {
    next(error);
  }
})

const UserModel = mongoose.model.UserModel || mongoose.model('UserModel', UserSchema);

export default UserModel;