import createHttpError from "http-errors";
import UserModel from "../models/userModel.js";
import { createUser, signUser } from "../services/auth.service.js";
import { generateToken, verifyToken } from "../services/token.service.js";
import { findUserById } from "../services/user.service.js";

export const register = async (req, res, next) => {
  try {
    const {name, email, picture, status, password} = req.body

    const newUser = await createUser({name, email, picture, status, password})

    const accessToken = await generateToken(
      {userId: newUser._id}, 
      "1d",
      process.env.ACCESS_TOKEN_SECRET
    )

    const refreshToken = await generateToken(
      {userId: newUser._id}, 
      "30d",
      process.env.REFRESH_TOKEN_SECRET
    )

    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      path: '/api/v1/auth/refreshToken',
      maxAge: 30 * 24 * 60 * 60 * 1000 // 30 days
    })

    res.status(201).json({
      message: 'register success.',
      user: {
        _id: newUser._id,
        name: newUser.name,
        email: newUser.email,
        picture: newUser.picture,
        status: newUser.status,
        token: accessToken
      },
    });

  } catch (error) {
    next(error)
  }
}

export const login = async(req, res, next) => {
  try {
    const {email, password} = req.body;
    
    const user = await signUser(email, password);
    
    const accessToken = await generateToken(
      {userId: user._id},
      "1d",
      process.env.ACCESS_TOKEN_SECRET
    )

    const refreshToken = await generateToken(
      {userId: user._id},
      "30d",
      process.env.REFRESH_TOKEN_SECRET
    )

    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      path: '/api/v1/auth/refreshToken',  
      maxAge: 30 * 24 * 60 * 60 * 1000 // 30 days
    })

    res.json({
      message: 'register success.',
      user: {
        _id: user._id,
        name: user.name,
        email: user.email,
        picture: user.picture,
        status: user.status,
        token: accessToken,
      },
    });

  } catch (error) {
    next(error);
  }
};

export const logout = async (req, res, next) => {
  try {
    res.clearCookie('refreshToken', {path: '/api/v1/auth/refreshToken'});
    res.json({message: 'logged out !'});
  } catch (error) {
    next(error);
  }
};

export const refreshToken = async (req, res, next) => {
  try {
    const { refreshToken } = req.cookies;

    if (!refreshToken) {
      throw createHttpError.Unauthorized('Please login.');
    }

    const check = await verifyToken(refreshToken, process.env.REFRESH_TOKEN_SECRET);

    const user = await findUserById(check.userId);

    const accessToken = await generateToken(
      {userId: user._id},
      "1d",
      process.env.ACCESS_TOKEN_SECRET
    )

    res.json({
      user: {
        _id: user._id,
        name: user.name,
        email: user.email,
        picture: user.picture,
        status: user.status,
        token: accessToken,
      },
    });
    
  } catch (error) {
    next(error);
  }
};