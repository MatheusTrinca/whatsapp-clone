import app from './app.js';
import logger from './configs/logger.config.js';
import mongoose from 'mongoose';

// env variables
const PORT = process.env.PORT || 8000;
const { DATABASE_URL } = process.env;

// exit on mongodb error
mongoose.connection.on('error', (err) => {
  logger.error(`MongoDB connection error: ${err}`);
  process.exit(1);
})

// mongodb connection
mongoose.connect(DATABASE_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  logger.info('MongoDB connected.')
});

// mongodb debug mode
if(process.env.NODE_ENV !== 'production') {
  mongoose.set("debug", true)
}

const server = app.listen(PORT, () => {
  logger.info(`Server running at port ${PORT}...`)
});

// handle server errors
const exitHandler = () => {
  if(server) {
    logger.info('Server closed.')
    process.exit(1)
  } else{
    process.exit(1)
  }
}

const unexpectedErrorHandler = error => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

// SIGTERM
process.on('SIGTERM', () => {
  if(server) {
    logger.info('Server closed.');
    process.exit(1);
  }
})