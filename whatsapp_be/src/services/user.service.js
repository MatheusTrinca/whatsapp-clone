import createHttpError from 'http-errors';
import { UserModel } from '../models/index.js';

export const findUserById = async id => {
  const user = await UserModel.findById(id);
  
  if(!user) throw createHttpError.NotFound('User not found');
  
  return user;
};
