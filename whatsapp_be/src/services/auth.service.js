import createHttpError from "http-errors";
import validator from "validator";
import { UserModel } from "../models/index.js";
import bcrypt from "bcryptjs"

export const createUser = async (userData) => {

  const { DEFAULT_PICTURE, DEFAULT_STATUS } = process.env;
  
  const {name, email, picture, status, password} = userData

  // check if fields are empty
  if(!name || !email || !password){
    throw createHttpError.BadRequest("Please fill all fields")
  }

  // check name length
  if(!validator.isLength(name, {min: 2, max: 25})){
    throw createHttpError.BadRequest("Name must be between 2 and 25 characters")
  }
  
  // check status length
  if(status?.length > 64){
    throw createHttpError.BadRequest("Status must be less than 64 characters")
  }

  // check if email address is valid
  if(!validator.isEmail(email)){
    throw createHttpError.BadRequest("Please provide a valid email address")
  }

  //check if user already exist
  const checkDb = await UserModel.findOne({email})

  if(checkDb){ 
    throw createHttpError.Conflict("Please try again with a different email address. This email already exists")
  }

  //check password length
  if(!validator.isLength('password', {
    min: 6,
    max: 128
  })) {
    throw createHttpError.BadRequest("Password must be between 6 and 128 characters")
  }

  //hash password--->to be done in the user model

  //adding user to databse
  const user = await new UserModel({
    name,
    email,
    picture: picture || DEFAULT_PICTURE,
    status: status || DEFAULT_STATUS,
    password
  }).save()

  return user

}

export const signUser = async (email, password) => {
  const user = await UserModel.findOne({email: email.toLowerCase()}).lean()

  if(!user) {
    throw createHttpError.BadRequest("Invalid credentials.")
  }

  const passwordMatch = await bcrypt.compare(password, user.password)

  if(!passwordMatch) {
    throw createHttpError.BadRequest('Invalid credentials.');
  }

  return user
}