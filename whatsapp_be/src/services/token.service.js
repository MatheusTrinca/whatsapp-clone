import { check, sign } from "../utils/token.util.js"

export const generateToken = async (payload, expiresIn, secret) => {
  let token = await sign(payload, expiresIn, secret);
  return token;
}

export const verifyToken = async (token, secret) => {
  let payload = await check(token, secret);
  return payload;
}