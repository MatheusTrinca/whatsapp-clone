import express from 'express';
import {
  register, login, logout, refreshToken
} from '../controllers/auth.controller.js'
import trimRequest from 'trim-request';

const router = express.Router();

router.use('/register', trimRequest.all, register);
router.use('/login', trimRequest.all, login);
router.use('/logout',trimRequest.all, logout);
router.use('/refreshToken',trimRequest.all, refreshToken);

export default router;
